import cards from "@firtoska/cards";
import DoolLogic from "@dool/logic";

class Undo {
  constructor(player, rejected, accepted, mode) {
    this.player = player;
    this.rejected = rejected;
    this.accepted = accepted;
    this.mode = mode;
  }
}

function Dool(data, client) {
  // Inherit behaviour and functions from DoolLogic
  DoolLogic.apply(this, {})

  // Hack: the data property isn't being set so
  // we're setting it again
  this.data = data

  // A client for sending and receiving messages
  // from the server. This is assumed to handle
  // all the messages in XMPP/JSON/whatever format
  // is being used
  this.client = client

  // if the game ends, we need to set this to true
  // Will help in resetting deck and tricks
  this.data.gameEnd = false

  // for letting the client to connect first
  this.client?.xmpp?.on("online", () => {
    // It needs to be changed to check JID (not nickname)
    if (this.data.occupants && client.nickname == this.data.occupants.find((occ) => occ.nickname == client.nickname).nickname) {
      client.join(this.gameID, null, client.nickname)
    }
  });

  window.addEventListener('online', () => {
    // It needs to be changed to check JID (not nickname)
    if (client.xmpp.status == "online" && this.data.occupants && client.nickname == this.data.occupants.find((occ) => occ.nickname == client.nickname).nickname) {
      client.join(this.gameID, null, client.nickname)
    }
  });

  window.addEventListener('offline', () => {
    this.data.error = {};
    this.data.error.message = 'Trying to rejoin, you got disconnected :(';
  });

  this.client.on("connected", (online) => {
    // It needs to be changed to check JID (not nickname)
    if (this.data.occupants && client.nickname == this.data.occupants.find((occ) => occ.nickname).nickname) {
      client.join(this.gameID, null, client.nickname)
    }
  })

  /**
   * This is for future error handling
   */
  this.client.on("error", (err) => {
    // TODO: add error handling code here
  })

  // Handle client events
  this.client.on("start", ({ game, user }) => {
    console.log(`${user} is ready to play at ${game}`)

    // reset the data for the next game, if it exists?!
    if (this.data.players || this.data.bids || this.data.contractLevel || this.data.declarer) {
      delete this.data.players
      delete this.data.scorePlayers
      delete this.data.bids
      delete this.data.contractLevel
      delete this.data.declarer
      delete this.data.scores
      delete this.data.contract
      delete this.data.leader
      delete this.data.fold
      delete this.data.runningScore
      delete this.data.gameFinished

      // setting gameEnd value for resetting data
      this.data.gameEnd = true
      this.data.deck = undefined
      this.data.tricks = new cards.Tricks(this.data.deck)
      // clear the session storage!
      sessionStorage.clear();
    }

    this.data.occupants = this.data.occupants.filter(
      (o) => !o.inactive
    )

    // Update the user
    let userIndex = this.data.occupants.findIndex((o) => o.nickname == user)

    // If the user is there, add them in
    if (userIndex >= 0) {
      this.data.occupants[userIndex].ready = true
      this.data.occupants[userIndex].role = "player"
      return;
    } else {
      console.warn(`Got start message from non-existent user ""${user}"!`)
    }
  });

  this.client.on("roomJoin", (presence) => {
    // Add user to occupant list if needed
    if (!this.data.occupants) {
      this.data.occupants = [];
    }

    // TODO: if it is not commented out, giving error in kibitzer :)
    // if (this.data.players !== undefined) {
    //   this.data.players = undefined // or delete the players - delete this.data.players
    // }

    let userIndex = this.data.occupants.findIndex(
      (o) => o.nickname == presence.nickname
    );

    let playerName = this.data.occupants[userIndex]?.nickname

    // If user is already there, just update
    if (userIndex >= 0) {
      this.data.occupants[userIndex].role = presence.role
      this.data.occupants[userIndex].jid = presence.user

      // delete the particular `rejoined occupant's`
      // `inactive` status, if it is there
      if (this.data.occupants && this.data.occupants[userIndex].inactive) {
        delete this.data.occupants[userIndex].inactive
      }
      if (this.data.players) {
        this.data.players.forEach((player) => {
          if (playerName == player.nickname) {
            delete player.inactive
          }
        })
      }
      return
    }

    // Otherwise, add
    this.data.occupants.push({
      jid: presence.user.toString(),
      role: presence.role,
      nickname: presence.nickname,
    })
  })

  this.client.on("roomDisconnect", (presence) => {
    // FIXME: only do this for exit, not disconnect!
    // Remove the user from the occupants list

    if (!this.data.occupants) {
      this.data.occupants = []
    }

    let userIndex = this.data.occupants.findIndex(
      (o) => o.jid == presence.user.toString()
    );

    // If the user is there, remove them
    if (userIndex >= 0) {
      let removedUser = this.data.occupants[userIndex]

      let playerName = this.data.occupants[userIndex]?.nickname
      // setting the player to inactive
      // if (this.data.players) {
      //   this.data.players.forEach((player) => {
      //     if (playerName == player.nickname) {
      //       player.inactive = true
      //     }
      //   })
      // }

      // change the inactive flag to true, if the user is a player
      // this.removePlayer(removedUser.nickname)
    }

    sessionStorage.clear()
  });

  // for removing players if anyone exited in the midgame
  this.removePlayer = (nickname) => {
    // check for the players array
    if (!this.data?.players || !this.data.players.length) return

    // finding out which are the players need ot be removed!
    // get their indices based on the nickname
    let playerIndices = this.data.players.reduce((indices, p, index) => {
      if (p.nickname === nickname) {
        indices.push(index);
      }
      return indices;
    }, []);

    playerIndices.forEach((playerIndex) => {
      if (this.data.take && playerIndex === this.data.take.player) {
        this.data.take.enable = false
      }

      // changing the inactive flag to true
      this.data.players[playerIndex].inactive = true;
    });

    // if all the other players are inactive means game over
    let activePlayers = this.data.players.filter((p) => !p.inactive)

    // if only one active player is left means game over
    // if (activePlayers.length === 2) this.data.gameOver = true

    return
  };

  this.client.on("roomExit", (presence) => {
    // Remove the user from the occupants list
    if (!this.data.occupants) {
      this.data.occupants = []
    }

    let userIndex = this.data.occupants.findIndex(
      (o) => o.jid == presence.user.toString()
    );

    // If the user is there, remove them
    if (userIndex >= 0) {
      let removedUser = this.data.occupants[userIndex]
      // this.data.occupants[userIndex].inactive = true

      // change the inactive flag to true, if the user is a player
      // this.removePlayer(removedUser.nickname)
    }

    sessionStorage.clear()
  });

  // Handle state updates
  this.client.on("state", (state) => {
    // Okay, so there's a lot to unpack here; let's take this
    // slowly.

    // Start with the deck, since that's the most important
    if (state.deck) {
      // setting false because it will reset the value to false
      this.data.gameEnd = false

      if (!this.data.deck) {
        this.data.deck = new cards.Deck();
      }

      if (!this.data.bids) {
        this.data.bids = []
      }
      this.data.deck.cards = state.deck;
    }

    // idk, I think it is for resetting the players and dealers at end of the game :(
    // ^--- see that's why you should've commented earlier, while writing
    // the code

    // For setting side value to the player:
    function getSide(index) {
      const sides = ['S', 'W', 'N', 'E'];
      return sides[index % sides.length];
    }

    // Now comes the major part: the players!
    if (state.players) {
      let playingOccupants = this.data.occupants.filter((o) =>
        (o.role.slice(0, 6) == "player" || o.role.slice(0, 6) == "rover") && o.ready)

      if (!this.data.players) {

        this.data.players = []
        this.data.dupPlayers = []

        // Automatically compute the nicknames (this can be
        // overridden later

        // let playingOccupants = this.data.occupants.filter((o) =>
        //   o.role.slice(0, 6) == "player" && o.ready)
        //     \________/
        //          \
        //  _________\_________
        // /                   \
        // Forward compatibility: if we decide to make player
        // roles something like 'player-0', 'player-1', etc.
        // later, this logic will still work.

        // A rudimentary check: if there are more players in
        // the list than we have track of occupants, it means
        // there's something wrong and we should probably just
        // wait for the backend to tell us what the names are.

        // correct @badri provide a perfect comment
        // in dool we have to wait for backend to send nickname

        playingOccupants.forEach((o, i) => {
          this.data.players.push({
            nickname: o.nickname,
            jid: o.jid,
            team: o.team
          });
          this.data.dupPlayers.push({
            nickname: o.nickname,
            jid: o.jid,
            team: o.team
          });
        });
      }

      // Now we loop over the data that's just come in
      state.players.forEach((player, i) => {
        // If the player is undefined, it means there's
        // no data, so we can just skip it
        if (!player) return;

        // Now let's make sure we have a record for the
        // player, and see what's there to extract from it.
        if (!this.data.players[i]) {
          this.data.players[i] = {}
        }

        if (!this.data.dupPlayers[i]) {
          this.data.dupPlayers[i] = {}
        }

        // Get the nickname
        if (player.nickname) {
          this.data.players[i].nickname = player.nickname
          this.data.dupPlayers[i].nickname = i == 2 ? "partner1" : i == 3 ? "partner2" : player.nickname
          this.data.players[i].index = i
        }

        if (player.team) {
          this.data.players[i].team = player.team
          this.data.dupPlayers[i].team = player.team
        }

        if (player.swap) {
          this.data.players[i].swap = player.swap
        } else {
          console.log("do nothing eat 5 star!")
        }

        // Get the hand
        if (player.hand) {
          this.data.players[i].hand = player.hand;
          this.data.dupPlayers[i].hand = player.hand;
          // Sort the cards!
          if (!this.data.deck) this.data.deck = new cards.Deck()
          this.data.players[i].hand.sort(this.data.deck.compare)
          this.data.dupPlayers[i].hand.sort(this.data.deck.compare)
        }

        // exposed means player left the room
        if (player.exposed) {
          this.data.players[i].exposed = player.exposed
        }

        // That's it for the loop! This comment is just there
        // to remind you what the following close-brackets are
        // all about ----.----.
        //               |    |
      }) // <------------'    |


      // creating teams inside occupants
      this.data.occupants.forEach((occupant) => {
        occupant.players = [];

        // Find players with the same nickname in the playersArray using forEach
        state.players.forEach((player, index) => {
          if (player.nickname === occupant.nickname) {
            occupant.players.push(index);
          }
        });
      })

      // creating a teams structure to access the players
      // who are in same players key.
      this.data.teams = this.data.occupants
        .filter(occupant => occupant.players)
        .map(occupant => occupant.players);

      this.data.players = this.data.players.map((player, index) => ({ ...player, side: getSide(index) }));
      this.data.dupPlayers = this.data.dupPlayers.map((player, index) => ({ ...player, side: getSide(index) }));
    } // <--------------------'

    // Create a scorePlayer array
    if (state.scorePlayers) {
      this.data.scorePlayers = JSON.parse(JSON.stringify(this.data.players))

      state.scorePlayers?.forEach((player, i) => {
        if (!player) return;

        if (!this.data.scorePlayers[i]) {
          this.data.scorePlayers[i] = {}
        }

        if (player.nickname) {
          this.data.scorePlayers[i].nickname = player.nickname
        }

        // Get the hand
        if (player.hand) {
          this.data.scorePlayers[i].hand = player.hand;
          // Sort the cards!
          if (!this.data.deck) this.data.deck = new cards.Deck()
          this.data.scorePlayers[i].hand.sort(this.data.deck.compare)
        }
      })
    }


    if (state.dealer) {
      // set the dealer value from the backend!
      this.data.dealer = +state.dealer
    }

    // Time to process the bids. We'll assume the entire set of bidInfo
    // sent at once; there's no "partial bids update like in Hool!"
    if (state.bids) {
      if (!this.data.bids) {
        this.data.bids = []
      }

      // assigning state bids to data
      this.data.bids = state.bids
    }

    if (state.runningScore) {
      this.data.runningScore = state.runningScore
    }

    // setting score and cum score
    if (state.scores) {
      // set gameFinished
      this.data.gameFinished = true

      // resetting showClaim 
      this.data.showClaim = false

      // once the score comes, we have to show all 4 players hands
      // so re-iterate the tricks and set it into the respective
      // players hands
      // const flattenedTricks = this.data.tricks.getTricksArray()

      // this.data.scorePlayers = JSON.parse(JSON.stringify(this.data.players))

      // // Ensure each player in scorePlayers has a hand property,
      // // otherwise you will face an issue like cannot push to undefined
      // this.data.scorePlayers.forEach(player => {
      //   player.hand = [];
      // });

      // console.log("in score ", flattenedTricks, this.data.scorePlayers)

      // flattenedTricks.forEach((trick) => {
      //   trick.forEach((trickEl) => {
      //     if (trickEl.card) {
      //       const player_index = trickEl.player;
      //       const card = trickEl.card;
      //       this.data.scorePlayers[player_index].hand.push({ ...card });
      //     }
      //   })
      // })

      // // sort the hands 
      // this.data.scorePlayers.forEach((player, i) => {
      //   if (player.hand) {
      //     this.data.scorePlayers[i].hand.sort(this.data.deck.compare)
      //   }
      // })

      if (!this.data.scores) {
        this.data.scores = state.scores;
      }
    }
    if (state.dealScores) {
      let result = state.dealScores.reduce(
        (acc, val) => {
          val.forEach((item, index) => {
            acc[index] = (+acc[index] || 0) + (+item);
          });
          return acc;
        },
        [0, 0]
      );
      console.log("Cumulative Scores", result);
      this.data.cumScore = result;
    }

    if (state.dealNumber !== undefined) {
      this.data.dealNumber = state.dealNumber;
      this.data.vulSides = this.getVulnerability(state.dealNumber);
      console.log("Deal Number", this.data.dealNumber);
      console.log("Vulnerability", this.data.vulSides);
    }
    // setting contract 
    if (state.contract) {
      if (this.data.bids.length >= 2) {
        this.data.contract = state.contract
        this.data.declarer = +state.contract.declarer
      }
    }

    // setting leader 
    if (state.leader) {
      // set the leader value from the backend!
      this.data.leader = +state.leader
    }

    // setting handLength 
    if (state.handLength) {
      // set the leader value from the backend!
      this.data.handLength = +state.handLength
    }

    // Time to process the tricks. We'll assume the entire set
    // of tricks is sent at once; there's no "partial tricks
    // update for that round" concept here!
    if (state.tricks) {
      // Again, we make sure there are tricks
      if (!this.data.tricks) {
        this.data.tricks = new cards.Tricks()
      }

      // set trump here if specified
      let trump = state.tricks.find((el) => el.name === "trump").children[0]
      this.data.tricks.trumpSuit = trump

      // Now we loop through them, but this is a significantly
      // smaller loop than last time.
      // loading the state.tricks to the Tricks instance!
      this.data.tricks.loadArray(
        state.tricks
          .filter(trick => Array.isArray(trick))
          .map(trick => {
            if (Array.isArray(trick)) {
              const trickElements = trick.map(trickEl => {
                if (trickEl.proposal) {
                  // If a proposal exists in the trick element, initiate the claim
                  const acceptedUsers = trickEl.accepted;
                  const rejectedUsers = trickEl.rejected;
                  // const participants = trick.map(el => el.player);
                  const claim = trickEl.proposal;
                  return new cards.Trick().addClaim(claim, acceptedUsers, rejectedUsers);
                } else {
                  // If no proposal, just format the card
                  return {
                    player: +trickEl.player,
                    card: {
                      rank: trickEl.rank,
                      suit: trickEl.suit
                    }
                  };
                }
              });
              return trickElements;
            }
          })
      );

      let participants = [0, 1]

      // adding claim instance to the tricks
      state.tricks.forEach((trick, i) => {
        if (this.data.tricks[i]) {
          trick.forEach((trickEl, j) => {
            if (trickEl.proposal) {
              const claimObj = new cards.Trick().addClaim(trickEl.proposal, trickEl.accepted, trickEl.rejected, participants);
              this.data.tricks[i][j] = claimObj
            }
          })
        }
      })

    }

    /**
     * If it is in a score stage - i.e., after score state sent,
     * we need to change the user ready status to false, otherwise
     * you cannot start the new game. After that delete the players
     * array also - if it is not done, it won't re-render the new user presence
     */

    if (this.getTurn().type == "score") {
      this.data.occupants.forEach((player) => {
        player.ready = false
      })
    }

    // for kibitz
    this.data.occupants.forEach((occupant) => {
      if (occupant?.players?.length == 0) occupant.role = "watcher"
    })
    // And that's it! We're all unpacked :D
  })

  this.client.on("bid", (bid) => {
    // Figure out which player made the bid
    if (!bid) console.log("Please get some bid object?!");

    if (!!bid) {
      this.data.bids.push({
        player: bid.player,
        bidType: bid.type,
        level: bid.level,
        suit: bid.suit
      })
    }

    // for setting the contract
    if (this.data.bids.length >= 2 && this.hasPassed()) {
      let contract = this.getContract()
      this.data.declarer = +contract?.declarer
      this.data.tricks.trumpSuit = contract?.suit == 'NT' ? undefined : contract.suit
      this.data.contract = contract
    }
  });

  this.getRunningScores = () => {
    let runningScores = this.data.players?.map((_) => 0);

    if (this.data?.tricks?.length > 0) {
      // Is the current trick complete?
      let skipLast = !this.isTrickComplete();

      this.data.tricks.forEach((_, i) => {
        // Skip the last trick if incomplete
        if (skipLast) {
          if (i >= this.data.tricks.length - 1) return;
        }

        const winningCard = this.data.tricks.getWinningCard(i + 1); // 1-indexed// Adding 1 to index to simulate trick number

        // Determine the winner's team
        const winnerTeam =
          winningCard.player === 0 || winningCard.player === 2 ? 0 : 1;

        // Increment the winner's score
        runningScores[winnerTeam] += 1;
      });
    }

    return runningScores;
  }

  this.client.on("card", (card) => {
    // Figure out which player played the card
    let player = card.player;

    // If it wasn't specified, deduce from nickname
    if (typeof card.player != "number") {
      card.player = this.data.players.findIndex(
        (p) => p.nickname == card.nickname
      )
    }

    // Play the card
    this.play(
      Number(player),
      {
        suit: card.suit,
        rank: card.rank,
      },
      false)  // <----.
    //  .-------------'
    //  |
    // That "false" means we don't try to remove the
    // card from the hand if we don't have the hand
    // in the first place. (We don't have other players'
    // hands, so we shouldn't try to see if they have
    // a card, remove it, etc. If we *do* have the hand,
    // eg. our own hand, then it will get processed as
    // usual).

    // TODO: A

    if (this.isTrickComplete())
      this.data.runningScore = this.getRunningScores()
  });

  // this is for take of flip
  this.client.on("take", (take) => {
    // Figure out which player took the trick
    let player = take.player

    // If it wasn't specified, deduce from nickname
    if (typeof take.player != "number") {
      player = this.data.players.findIndex(
        (p) => p.nickname == take.nickname
      )
    }

    this.take(player)
  })

  // this is for setting leading player to start the card
  this.client.on("leading", (lead) => {
    // Figure out which player made the decision
    let player = lead.player // the declarer

    // If it wasn't specified, deduce from nickname
    if (typeof lead.player != "number") {
      player = this.data.players.findIndex(
        (p) => p.nickname == lead.nickname
      )
    }

    // set the side of the player - who needs to start
    // the decision made by declarer!
    this.data.leader = +lead.side
  })

  //chat handler for incoming chat
  this.client.on("chat", (chat) => {
    if (!this.data.messages) {
      this.data.messages = []
    }

    this.data.messages.push({
      from: chat.nickname,
      text: chat.text,
    })
  })

  // handler for fold
  this.client.on("fold", (fold) => {
    this.data.fold = true
  })

  // get the claimObj index, which will have the claim Class
  this.findLastClaimIndex = (trick) => {
    for (let i = trick.length - 1; i >= 0; i--) {
      if (trick[i].proposal) {
        return i;
      }
    }
    return null; // Return -1 if no proposal is found
  }

  this.findLastUndoIndex = (trick) => {
    for (let i = trick.length - 1; i >= 0; i--) {
      if (trick[i].mode) {
        return i;
      }
    }
    return null; // Return -1 if no proposal is found
  }

  // Compare the given objects has the same proposal
  this.compareProposals = (obj1, obj2) => {
    const keys1 = Object.keys(obj1);
    const keys2 = Object.keys(obj2);

    if (keys1.length !== keys2.length) {
      return false;
    }

    for (let key of keys1) {
      if (obj1[key] !== obj2[key]) {
        return false;
      }
    }

    return true;
  }

  this.client.on("claim", (claim) => {
    // Again, we make sure there are tricks
    if (!this.data.tricks) {
      this.data.tricks = new cards.Tricks()
    }

    if (this.data.tricks.length > 0) {
      let trick = this.data.tricks[this.data.tricks.length - 1]

      // which will have the matching claim object
      let claimIndex = this.findLastClaimIndex(trick);
      if (
        // implement claimClass (matchClass) && activeClaim && compare proposal
        this.findLastClaimIndex(trick) !== null
        && trick[claimIndex].isClaimGoingOn()
        && this.compareProposals(trick[claimIndex].proposal, claim.proposal)
      ) {
        if (claim.mode === "rejected") {
          trick[claimIndex].rejectedBy.push(claim.player)
        } else {
          trick[claimIndex].acceptedBy.push(claim.player)
        }
      } else if (
        trick.some((claim) => claim.proposal && claim.isClaimGoingOn())
      ) {
        return `There is a claim in progress!`
      } else {
        let acceptedUsers = [claim.player];
        // create the rejected array:
        let rejectedUsers = []

        let participants = []

        this.data.players.forEach((player, index) => {
          if (!player.inactive) {
            participants.push(index);
          }
        });

        // TODO: Don't do this, do something else
        participants.splice(-2)

        // first add the claimObj in currentTrick
        trick.push(new cards.Trick().addClaim(claim.proposal, acceptedUsers, rejectedUsers, participants))
      }
    }
  })

  this.getTeam = (player) => {
    return this.data.teams.find(team => team.includes(player))
  }

  // this is for swap
  this.client.on("swap", (swap) => {
    // Figure out which player took the trick
    let player = +swap.player
    let myTeam = this.getTeam(player)

    // make the players swap value to true
    this.data.players.forEach((player, index) => {
      if(myTeam.includes(index)) {
        this.data.players[index].swap = true
      }
    })
  })

  // this is for undo
  this.client.on("undo", (undo) => {

    // Again, we make sure there are tricks
    if (!this.data.tricks) {
      this.data.tricks = new cards.Tricks()
    }

    if (this.data.tricks.length > 0) {
      let currentTrick = this.data.tricks[this.data.tricks.length - 1]
      let prevTrick = this.data.tricks.length > 1 ? this.data.tricks[this.data.tricks.length - 2] : undefined

      console.log("current trick from dool ", currentTrick, prevTrick)

      // which will have the matching claim object
      let undoIndex = this.findLastUndoIndex(currentTrick.length ? currentTrick : prevTrick);

      if (currentTrick.getCards().length == 0) {

        console.log("going in prevTrick mode!", undoIndex)
        if (undo.mode === "rejected") {
          prevTrick[undoIndex].rejected.push(undo.player)
          prevTrick[undoIndex].mode = "rejected"
        } else if (undo.mode === "accepted") {
          undoIndex = this.findLastUndoIndex(prevTrick)
          prevTrick[undoIndex].accepted.push(undo.player)
          prevTrick[undoIndex].mode = "accepted"

          console.log("undo INdex in PrevTrick ", undoIndex)
          // Remove the newly added trick
          this.data.tricks.pop()

          // Call undo for removing the last card!
          for(let i = prevTrick.length - 1; i >= 0; i--) {
            if(prevTrick[i].card) {
              prevTrick.splice(i, 1);
              break;
            }
          }

          undoIndex = this.findLastUndoIndex(prevTrick)

          // put back the card to the particular player
          if (!this.getTeam(undo.card.player).includes(+undo.player)) {
            console.log("PrevTrick: push the card only to the approp player!")
            if (this.data.players[prevTrick[undoIndex].player].hand) {
              this.data.players[prevTrick[undoIndex].player].hand?.push({ suit: undo.card.suit, rank: undo.card.rank })
              // sort the hands
              this.data.players[prevTrick[undoIndex].player].hand?.sort(this.data.deck.compare)
            } else {
              console.log("no hands to push in prevTrick!")
            }
          } else {
            console.log("do nothing in prevTrick!")
          }
        } else if (undo.mode === "request") {
          let rejected = []
          let accepted = []

          // Create an undo instance
          prevTrick.push(new Undo(undo.card.player, rejected, accepted, undo.mode))
        }
      } else {
        if (undo.mode === "rejected") {
          currentTrick[undoIndex].rejected.push(undo.player)
          currentTrick[undoIndex].mode = "rejected"
        } else if (undo.mode === "accepted") {
          currentTrick[undoIndex].accepted.push(undo.player)
          currentTrick[undoIndex].mode = "accepted"

          if(currentTrick[currentTrick.length - 2].mode == "accepted") {
            return;
          };

          for(let i = currentTrick.length - 1; i >= 0; i--) {
            if(currentTrick[i].card) {
              currentTrick.splice(i, 1);
              break;
            }
          }

          undoIndex = this.findLastUndoIndex(currentTrick)

          console.log("this is what i know ", this.getTeam(undo.card.player), typeof undo.player)
          // put back the card to the particular player
          if (!this.getTeam(undo.card.player).includes(+undo.player)) {
            console.log("Normal: push the card only to the approp player!")
            if (this.data.players[currentTrick[undoIndex].player].hand) {
              this.data.players[currentTrick[undoIndex].player].hand?.push({ suit: undo.card.suit, rank: undo.card.rank })
              // sort the hands
              this.data.players[currentTrick[undoIndex].player].hand?.sort(this.data.deck.compare)
            } else {
              console.log("no hands to push!")
            }
          } else {
            console.log("do nothing!")
          }
        } else if (undo.mode === "request") {
          let rejected = []
          let accepted = []

          // Create an undo instance
          currentTrick.push(new Undo(undo.card.player, rejected, accepted, undo.mode))
        }
      }
    }
  })
}

export default Dool
