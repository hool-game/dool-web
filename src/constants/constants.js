export const MESSAGES = {
    SWAP_HEADER: "Do you wish to swap the cards?",
    SWAP_INNER_TEXT: "Tap to swap one card between your two hands",
    BID: "bid",
    DECIDE_SWAP: " is deciding on Swap",
    SWAP_INSTRUCT_1: "Please click on one card that you wist to swap on each side",
    PREVIEW_MSG: "Preview of your hands after swap",
    UNDO_INNER_TEXT: "Do you accept the undo?",
}

export const GAME_MESSAGES = {
    TURN: "It is your turn to play",
}

export const BUTTONS = {
    PREVIEW: "Preview",
    UNDO: "Undo",
    AGREE: "Swap",
    CANCEL: "Cancel",
    PASS: "Pass",
    CLAIM: "Claim",
    CANCEL_X: "X",
    CLEAR: "Clear",
    NO_SWAP: "No Swap",
    ACCEPT: "Accept",
    REJECT: "Reject",
}

export const SWAP = {
    SWAP_CARD_COUNT: 1
}