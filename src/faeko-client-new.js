import { xml, jid, client } from "@xmpp/client";
import EventEmitter from "events";

export class DoolClient extends EventEmitter {
    constructor(options = {}, ...args) {
        // set the standard
        options.standard = "http://hool.org/protocol/mug/dool";

        super(...args);
        if (!options.service) {
            console.warn("No XMPP service specified. We won't be able to connect!");
        } else {
            this.service = options.service;
        }
        if (!options.standard) {
            console.warn(
                "No game standard specified. Using generic Multi-User Gaming, which may include incompatible game hosts"
            );
            this.standard = "http://jabber.org/protocol/mug";
        } else {
            this.standard = options.standard;
        }
        this.xmpp = null;
        this.jid = null;
        this.resource = options.resource || Math.random().toString(16).substr(2, 8);
    }

    /**
     * Game-level listeners
     */
    handleGamePresence(presence) {
        let game = presence.getChild("game");

        if (game.attrs.xmlns == "http://jabber.org/protocol/mug") {
            // Handle game errors
            if (presence.getChildren("error").length) {
                let result = {
                    room: jid(presence.attrs.from).bare(),
                    user: jid(presence.attrs.to).bare(),
                    error: {},
                };
                let error = presence.getChild("error");

                if (error.attrs.type == "wait") {
                    result.error.type = "wait";

                    // Handle "room full" event
                    if (error.getChildren("service-unavailable").length) {
                        if (
                            error.getChild("service-unavailable").attrs.xmlns ==
                            "urn:ietf:params:xml:ns:xmpp-stanzas"
                        ) {
                            result.event = "roomFull";
                            return result;
                        }
                    }
                }
            }

            // Handle state update
            let status;
            if (game.getChildren("status").length) {
                status = game.getChild("status");
            }

            if (status || game.getChildren('state').length) {
                let state = {};
                state.room = jid(presence.attrs.from).bare().toString();

                if (status) state.status = status.text();
                let xData = game.getChild('state').getChildren('x').find(x => x.attrs.xmlns == 'jabber:x:data');
                if (xData) {

                    if (!!xData.getChildren('deck').length) {
                        let deckEl = xData.getChild('deck');
                        state.deck = [];

                        if (deckEl.attrs.trump) {
                            state.trump = deckEl.attrs.trump;
                        }

                        deckEl.children.forEach(cardEl => state.deck.push({
                            suit: cardEl.attrs.suit,
                            rank: cardEl.attrs.rank
                        }));
                    }

                    if (!!xData.getChildren('players').length) {
                        let playersEl = xData.getChild('players');
                        state.players = [];
                        console.log("playerEl ", playersEl)
                        playersEl.getChildren('player').forEach((playerEl, i) => {
                            let player = {};
                            if (playerEl.attrs.nickname) {
                                player.nickname = playerEl.attrs.nickname
                            }
                            if (playerEl.attrs.team) {
                                player.team = playerEl.attrs.team
                            }
                            if (playerEl.attrs.inactive == 'true') {
                                player.inactive = true;
                            } else if (playerEl.attrs.inactive == 'false') {
                                player.inactive = false;
                            }

                            if (playerEl.attrs.swap) {
                                player.swap = Boolean(playerEl.attrs.swap)
                            }

                            if (playerEl.getChildren('hand').length) {
                                let handEl = playerEl.getChild('hand');
                                player.hand = [];

                                if (handEl.attrs.exposed == 'true') {
                                    player.exposed = true;
                                } else if (handEl.attrs.exposed == 'false') {
                                    player.exposed = false;
                                }

                                handEl.getChildren('card').forEach(c => player.hand.push({
                                    suit: c.attrs.suit,
                                    rank: c.attrs.rank
                                }));
                            }

                            if (!!playerEl.getChildren('bid').length) {
                                let bids = [];
                                playerEl.getChildren('bid').forEach(bidEl => {
                                    bidEl.children.forEach(bid => {
                                        bids.push({
                                            round: bid.attrs.round,
                                            side: bid.attrs.side,
                                            type: bid.attrs.type,
                                            level: bid.attrs.level,
                                            suit: bid.attrs.suit
                                        });
                                    });
                                });
                                state.bids = bids;
                            }

                            if (player.index) i = player.index;

                            state.players[i] = player;
                        });

                        if (!!xData.getChildren('tricks').length) {
                            let tricksEl = xData.getChild('tricks');
                            state.tricks = [];
                            state.flippedTricks = [];
                            if (tricksEl.getChildren('trick').length) {
                                tricksEl.getChildren('trick').forEach(trickEl => {
                                    let trick = [];
                                    if (trickEl.children.length > 0) {
                                        trickEl.children.forEach((childEl, i) => {
                                            if (childEl.name === 'card') {
                                                trick.push({
                                                    suit: childEl.attrs.suit,
                                                    rank: childEl.attrs.rank,
                                                    player: childEl.attrs.player
                                                });
                                            } else if (childEl.name === 'claim') {
                                                let proposalObj = {};
                                                let acceptedBy = [];
                                                let rejectedBy = [];

                                                if (childEl.getChildren('proposal').length) {
                                                    childEl.getChildren('proposal').forEach(proposalEl => {
                                                        const { player, trickCount } = proposalEl.attrs;
                                                        // Populate the proposal object
                                                        proposalObj[player] = +trickCount;
                                                    });
                                                }
                                                if (childEl.getChildren('accepted').length) {
                                                    childEl.getChildren('accepted').forEach(acceptedEl => {
                                                        // Add accepted proposal to the accepted array
                                                        acceptedBy.push(+acceptedEl.children[0]);
                                                    });
                                                }
                                                if (childEl.getChildren('rejected').length) {
                                                    childEl.getChildren('rejected').forEach(rejectedEl => {
                                                        // Add rejected proposal to the rejected array
                                                        rejectedBy.push(+rejectedEl.children[0]);
                                                    });
                                                } else {
                                                    console.log("nothing to parse");
                                                }

                                                trick.push({
                                                    proposal: proposalObj,
                                                    accepted: acceptedBy,
                                                    rejected: rejectedBy
                                                });
                                            }
                                        });
                                    }
                                    state.tricks.push(trick);
                                });
                            }
                            let trump = tricksEl.getChildren('trump')[0];
                            state.tricks.push(trump);
                        }
                    }

                    if (!!xData.getChildren('scorePlayers').length) {
                        let scorePlayersEl = xData.getChild('scorePlayers');
                        state.scorePlayers = [];
                        scorePlayersEl.getChildren('player').forEach((playerEl, i) => {
                            let player = {};
                            if (playerEl.attrs.nickname) {
                                player.nickname = playerEl.attrs.nickname
                            }
                            if (playerEl.attrs.team) {
                                player.team = playerEl.attrs.team
                            }
                            if (playerEl.attrs.inactive == 'true') {
                                player.inactive = true;
                            } else if (playerEl.attrs.inactive == 'false') {
                                player.inactive = false;
                            }

                            if (playerEl.getChildren('hand').length) {
                                let handEl = playerEl.getChild('hand');
                                player.hand = [];

                                if (handEl.attrs.exposed == 'true') {
                                    player.exposed = true;
                                } else if (handEl.attrs.exposed == 'false') {
                                    player.exposed = false;
                                }

                                handEl.getChildren('card').forEach(c => player.hand.push({
                                    suit: c.attrs.suit,
                                    rank: c.attrs.rank
                                }));
                            }

                            if (player.index) i = player.index;

                            state.scorePlayers[i] = player;
                        });
                    }

                    if (!!xData.getChildren('bidinfo').length) {
                        let bidInfoEl = xData.getChild('bidinfo');
                        state.bidInfo = [];
                        bidInfoEl.getChildren('bid').forEach(bidEl => {
                            state.bidInfo.push({
                                player: bidEl.attrs.player,
                                bidType: bidEl.attrs.type,
                                level: bidEl.attrs.level,
                                suit: bidEl.attrs.suit,
                            })
                        });
                    }

                    if (!!xData.getChildren('scores').length) {
                        let scoreEl = xData.getChild('scores');
                        state.scores = [scoreEl.attrs.team1, scoreEl.attrs.team2]
                    }

                    if (!!xData.getChildren('contract').length) {
                        let contractEl = xData.getChild('contract');
                        state.contract = {}

                        state.contract = {
                            level: +contractEl.attrs.level,
                            suit: contractEl.attrs.suit,
                            double: contractEl.attrs.double,
                            redouble: contractEl.attrs.redouble,
                            declarer: +contractEl.attrs.declarer,
                        }
                    }

                    if (!!xData.getChildren('score').length) {
                        let scoreEl = xData.getChild('score');
                        state.runningScore = [scoreEl.attrs.team1, scoreEl.attrs.team2]
                    }

                    if (!!xData.getChildren('dealer').length) {
                        let dealerEl = xData.getChild('dealer');
                        state.dealer = dealerEl.attrs.dealer
                    }

                    if (!!xData.getChildren('leader').length) {
                        let leaderEl = xData.getChild('leader');
                        state.leader = leaderEl.attrs.leader
                    }

                    if (!!xData.getChildren('handLength').length) {
                        let handLengthEl = xData.getChild('handLength');
                        state.handLength = handLengthEl.attrs.handLength
                    }
                }

                state.event = 'state';
                return state;
            }
        }

        return;
    }
    handleGameTurn(message) {
        let turn = message.getChild('turn');

        if (turn.getChildren('bid').length) {
            let bidEl = turn.getChild('bid');

            if (bidEl.attrs.xmlns != this.standard) return;

            if (message.getChildren('error').length) {
                let errorEl = message.getChild('error');
                let error = {};

                if (errorEl.getChildren('forbidden').length) {
                    error.type = 'not-authorised';
                } else if (errorEl.getChildren('invalid-turn').length) {
                    error.type = 'invalid-turn';
                } else {
                    error.type = 'unknown';
                }

                if (errorEl.getChildren('text').length) {
                    error.text = errorEl.getChild('text').text();
                }
                error.event = 'bidError';
                return error;
            }

            let bid = {};
            bid.room = jid(message.attrs.from).bare().toString();
            bid.nickname = jid(message.attrs.from).resource;
            bid.player = bidEl.attrs.player;
            bid.type = bidEl.attrs.type;
            bid.level = bidEl.attrs.level;
            bid.suit = bidEl.attrs.suit;

            bid.event = 'bid';
            return bid;
        }

        if (turn.getChildren('card').length) {
            let cardEl = turn.getChild('card');

            if (cardEl.attrs.xmlns != this.standard) return;

            if (message.getChildren('error').length) {
                let errorEl = message.getChild('error');
                let error = {};

                if (errorEl.getChildren('forbidden').length) {
                    error.type = 'not-authorised';
                } else if (errorEl.getChildren('invalid-turn').length) {
                    error.type = 'invalid-turn';
                } else {
                    error.type = 'unknown';
                }

                if (errorEl.getChildren('text').length) {
                    error.text = errorEl.getChild('text').text();
                }
                error.event = 'cardError';
                return error;
            }

            let card = {};
            card.room = jid(message.attrs.from).bare().toString();
            card.nickname = jid(message.attrs.from).resource;
            card.rank = cardEl.attrs.rank;
            card.suit = cardEl.attrs.suit;
            card.player = cardEl.attrs.player;
            card.event = 'card';
            return card;
        }

        if (turn.getChildren('take').length) {
            let takeEl = turn.getChild('take');

            if (takeEl.attrs.xmlns != this.standard) return;

            if (message.getChildren('error').length) {
                let errorEl = message.getChild('error');
                let error = {};

                if (errorEl.getChildren('forbidden').length) {
                    error.type = 'not-authorised';
                } else if (errorEl.getChildren('invalid-turn').length) {
                    error.type = 'invalid-turn';
                } else {
                    error.type = 'unknown';
                }

                if (errorEl.getChildren('text').length) {
                    error.text = errorEl.getChild('text').text();
                }
                error.event = 'takeError';
                return error;
            }

            let take = {};
            take.room = jid(message.attrs.from).bare().toString();
            take.nickname = jid(message.attrs.from).resource;
            take.event = 'take';
            return take;
        }

        if (turn.getChildren('flip').length) {
            let flipEl = turn.getChild('flip');

            if (flipEl.attrs.xmlns != this.standard) return;

            if (message.getChildren('error').length) {
                let errorEl = message.getChild('error');
                let error = {};

                if (errorEl.getChildren('forbidden').length) {
                    error.type = 'not-authorised';
                } else if (errorEl.getChildren('invalid-turn').length) {
                    error.type = 'invalid-turn';
                } else {
                    error.type = 'unknown';
                }

                if (errorEl.getChildren('text').length) {
                    error.text = errorEl.getChild('text').text();
                }
                error.event = 'flipError';
                return error;
            }

            let flip = {};
            flip.room = jid(message.attrs.from).bare().toString();
            flip.nickname = jid(message.attrs.from).resource;
            flip.event = 'flip';
            return flip;
        }

        if (turn.getChildren('leading').length) {
            let leadingEl = turn.getChild('leading');

            if (leadingEl.attrs.xmlns != this.standard) return;

            if (message.getChildren('error').length) {
                let errorEl = message.getChild('error');
                let error = {};

                if (errorEl.getChildren('forbidden').length) {
                    error.type = 'not-authorised';
                } else if (errorEl.getChildren('invalid-turn').length) {
                    error.type = 'invalid-turn';
                } else {
                    error.type = 'unknown';
                }

                if (errorEl.getChildren('text').length) {
                    error.text = errorEl.getChild('text').text();
                }
                error.event = 'leadingError';
                return error;
            }

            let leading = {};
            leading.room = jid(message.attrs.from).bare().toString();
            leading.nickname = jid(message.attrs.from).resource;
            leading.event = 'leading';
            leading.side = leadingEl.attrs.side
            return leading;
        }

        if (turn.getChildren('claim').length) {
            let claimEl = turn.getChild('claim');

            if (claimEl.attrs.xmlns != this.standard) return;

            if (message.getChildren('error').length) {
                let errorEl = message.getChild('error');
                let error = {};

                if (errorEl.getChildren('forbidden').length) {
                    error.type = 'not-authorised';
                } else if (errorEl.getChildren('invalid-turn').length) {
                    error.type = 'invalid-turn';
                } else {
                    error.type = 'unknown';
                }

                if (errorEl.getChildren('text').length) {
                    error.text = errorEl.getChild('text').text();
                }
                error.event = 'claimError';
                return error;
            }

            let claim = {};
            claim.room = jid(message.attrs.from).bare().toString();
            claim.nickname = jid(message.attrs.from).resource;
            claim.event = 'claim';
            claim.player = +claimEl.attrs.player;
            claim.mode = claimEl.attrs.mode

            // Initialize an empty proposal object
            claim.proposal = {};

            // Iterate over the children and populate the proposal object
            claimEl.getChildren('proposal').forEach(proposal => {
                claim.proposal[proposal.attrs.player] = parseInt(proposal.attrs.value);
            });

            return claim;
        }

        if (turn.getChildren('swap').length) {
            let swapEl = turn.getChild('swap');

            if (swapEl.attrs.xmlns != this.standard) return;

            if (message.getChildren('error').length) {
                let errorEl = message.getChild('error');
                let error = {};

                if (errorEl.getChildren('forbidden').length) {
                    error.type = 'not-authorised';
                } else if (errorEl.getChildren('invalid-turn').length) {
                    error.type = 'invalid-turn';
                } else {
                    error.type = 'unknown';
                }

                if (errorEl.getChildren('text').length) {
                    error.text = errorEl.getChild('text').text();
                }
                error.event = 'swapError';
                return error;
            }

            let swap = {};
            swap.room = jid(message.attrs.from).bare().toString();
            swap.nickname = jid(message.attrs.from).resource;
            swap.player = swapEl.attrs.player;
            swap.event = 'swap';
            return swap;
        }

        if (turn.getChildren('undo').length) {
            let undoEl = turn.getChild('undo');

            if (undoEl.attrs.xmlns != this.standard) return;

            if (message.getChildren('error').length) {
                let errorEl = message.getChild('error');
                let error = {};

                if (errorEl.getChildren('forbidden').length) {
                    error.type = 'not-authorised';
                } else if (errorEl.getChildren('invalid-turn').length) {
                    error.type = 'invalid-turn';
                } else {
                    error.type = 'unknown';
                }

                if (errorEl.getChildren('text').length) {
                    error.text = errorEl.getChild('text').text();
                }
                error.event = 'undoError';
                return error;
            }

            let undo = {};
            undo.room = jid(message.attrs.from).bare().toString();
            undo.nickname = jid(message.attrs.from).resource;
            undo.mode = undoEl.attrs.mode;
            const {rank, suit, player} = undoEl.getChild('card').attrs
            undo.card = {rank: rank, suit: suit, player: +player};
            undo.player = +undoEl.attrs.player;
            undo.event = 'undo';
            return undo;
        }
    }

    makeBid(gameID, player, bidType, bidLevel, bidSuit) {
        if (!gameID) throw 'Please specify a gameID';
        if (!player) throw 'Please specify the player you are bidding for';
        if (!bidType) throw 'Please specify the bid type';

        if (['level', 'redouble', 'double', 'pass'].indexOf(bidType) < 0) {
            throw `Invalid bid type: ${bidType}`;
        }

        if (bidType == 'level') {
            if (!bidLevel) throw 'Please specify level for level bid';
            if (!bidSuit) throw 'Please specify suit for level bid';
        }

        if (gameID.indexOf('@') < 0) gameID = `${gameID}@${this.gameService}`;
        this.xmpp.send(xml("message", {
            from: this.jid,
            to: gameID,
            type: "chat"
        }, xml("turn", {
            xmlns: "http://jabber.org/protocol/mug#user"
        }, xml("bid", {
            xmlns: "http://hool.org/protocol/mug/dool",
            player: player,
            type: bidType,
            level: bidLevel,
            suit: bidSuit
        }))));
    }

    bid(gameID, value) {
        if (!gameID) throw 'Please specify a gameID';
        if (typeof value != 'number') throw 'Please specify the bid value';

        if (gameID.indexOf('@') < 0) gameID = `${gameID}@${this.gameService}`;
        this.xmpp.send(xml("message", {
            from: this.jid,
            to: gameID,
            type: "chat"
        }, xml("turn", {
            xmlns: "http://jabber.org/protocol/mug#user"
        }, xml("bid", {
            xmlns: "http://hool.org/protocol/mug/dool"
        }, value))));
    }
    card(gameID, suit, rank, player = undefined) {
        if (!gameID) throw 'Please specify a gameID';
        if (!suit) throw 'Please specify a suit';
        if (!rank) throw 'Please specify a rank';

        if (gameID.indexOf('@') < 0) gameID = `${gameID}@${this.gameService}`;
        this.xmpp.send(xml("message", {
            from: this.jid,
            to: gameID,
            type: "chat"
        }, xml("turn", {
            xmlns: "http://jabber.org/protocol/mug#user"
        }, xml("card", {
            suit: suit,
            rank: rank,
            xmlns: this.standard
        }))));
    }
    take(gameID) {
        if (!gameID) throw 'Please specify a gameID';

        if (gameID.indexOf('@') < 0) gameID = `${gameID}@${this.gameService}`;
        this.xmpp.send(xml("message", {
            from: this.jid,
            to: gameID,
            type: "chat"
        }, xml("turn", {
            xmlns: "http://jabber.org/protocol/mug#user"
        }, xml("take", {
            xmlns: this.standard
        }))));
    }
    flip(gameID) {
        if (!gameID) throw 'Please specify a gameID';

        if (gameID.indexOf('@') < 0) gameID = `${gameID}@${this.gameService}`;
        this.xmpp.send(xml("message", {
            from: this.jid,
            to: gameID,
            type: "chat"
        }, xml("turn", {
            xmlns: "http://jabber.org/protocol/mug#user"
        }, xml("flip", {
            xmlns: this.standard
        }))));
    }
    setLeader(gameID, side) {
        if (!gameID) throw 'Please specify a gameID';
        if (!side) throw 'Please specify a side to set opening lead';

        if (gameID.indexOf('@') < 0) gameID = `${gameID}@${this.gameService}`;
        this.xmpp.send(xml("message", {
            from: this.jid,
            to: gameID,
            type: "chat"
        }, xml("turn", {
            xmlns: "http://jabber.org/protocol/mug#user"
        }, xml("leading", {
            side: side,
            xmlns: this.standard
        }))));
    }
    async xmppConnect(myJID, password = undefined) {
        this.jid = jid(myJID);
        if (this.xmpp) {
            await this.xmpp.stop();
        }
        if (!password) {
            this.xmpp = client({
                service: this.service,
                domain: myJID,
                resource: this.resource,
            });
        } else {
            this.xmpp = client({
                service: this.service,
                domain: this.jid.domain,
                resource: this.resource,
                username: this.jid.local,
                password: password,
            });
        }
        this.listen(this.xmpp);
    }
    async listen(connection) {
        const { iqCaller } = connection;
        connection.on("error", (err) => {
            let error = {};
            error.code = err.code;
            if (err.code == "ECONNERROR") {
                error.message = "Connection error. Please try again";
            } else if (err.condition == "not-authorized") {
                error.code = "not-authorized";
                error.message = "Invalid username or password";
            } else {
                error.message = "Unknown error";
            }
            this.emit("error", error);
        });
        connection.on("offline", () => {
            console.log("offline");
            this.emit("offline", {});
        });
        connection.on("stanza", async (stanza) => {
            console.log(`We got: ${stanza}`);
            let from = stanza.attrs.from ? jid(stanza.attrs.from) : undefined;
            if (from && from.local == this.jid.local && from.host == this.jid.host) {
                console.log("ignoring own message");
                return;
            }
            if (stanza.is("presence")) {
                if (stanza.getChildren("game").length) {
                    let e = this.handleGamePresence(stanza);
                    if (!!e) {
                        this.emit(e.event, e);
                        return;
                    } else {
                        let game = stanza.getChild("game");
                        let role;
                        let affiliation;
                        let room = jid(stanza.attrs.from).bare().toString();
                        let nickname = jid(stanza.attrs.from).resource;
                        let user = nickname;
                        if (game.getChildren("item").length) {
                            let item = game.getChild("item");
                            role = item.attrs.role;
                            affiliation = item.attrs.affiliation || `unknown`;
                            user = item.attrs.jid;
                        }
                        if (stanza.attrs.type == "unavailable") {
                            if (
                                game.getChildren("item").length &&
                                game.getChild("item").attrs.affiliation == "none"
                            ) {
                                this.emit("roomExit", {
                                    user: user,
                                    nickname: nickname,
                                    room: room,
                                });
                                return;
                            }
                            this.emit("roomDisconnect", {
                                user: user,
                                room: room,
                            });
                            return;
                        }
                        if (stanza.getChildren("error").length) {
                            let error = stanza.getChild("error");
                            this.emit("roomJoinError", {
                                room: room,
                                user: user,
                                nickname: nickname,
                                type: error.attrs.type || "unknown",
                                tag: error.children[0].name,
                                text: error.getChild("text").getText(),
                            });
                            return;
                        }
                        this.emit("roomJoin", {
                            room: room,
                            user: user,
                            role: role,
                            affiliation: affiliation,
                            nickname: nickname,
                        });
                        return;
                    }
                } else if (stanza.attrs.xmlns == "jabber:client") {
                    if (stanza.attrs.type == "subscribe") {
                        this.emit("roster-subscribe", {
                            user: stanza.attrs.from,
                        });
                    } else if (stanza.attrs.type == "subscribed") {
                        this.emit("roster-subscribed", {
                            user: stanza.attrs.from,
                        });
                    } else if (stanza.attrs.type == "unsubscribe") {
                        this.emit("roster-unsubscribe", {
                            user: stanza.attrs.from,
                        });
                    } else if (stanza.attrs.type == "unsubscribed") {
                        this.emit("roster-unsubscribed", {
                            user: stanza.attrs.from,
                        });
                    } else if (stanza.attrs.type == "unavailable") {
                        this.emit("contact-offline", {
                            user: stanza.attrs.from,
                        });
                    } else {
                        let show;
                        let status;
                        if (stanza.getChildren("show").length) {
                            show = stanza.getChild("show").text();
                        }
                        if (stanza.getChildren("status").length) {
                            status = stanza.getChild("status").text();
                        }
                        this.emit("contact-online", {
                            user: stanza.attrs.from,
                            show: show,
                            status: status,
                        });
                    }
                }
            }
            if (!stanza.is("message")) return;
            if (stanza.is("message")) {
                if (stanza.getChildren("turn").length) {
                    let e = this.handleGameTurn(stanza);
                    if (!!e) {
                        this.emit(e.event, e);
                        return;
                    }
                    return;
                }
                if (stanza.getChildren("start").length) {
                    let u = jid(stanza.attrs.from);
                    if (stanza.getChildren("error").length) {
                        this.emit("startError", {
                            game: `${u.bare()}`,
                            user: u.resource,
                        });
                    } else {
                        this.emit("start", {
                            game: `${u.bare()}`,
                            user: u.resource,
                        });
                    }
                }
                if (stanza.getChildren("fold").length) {
                    let fold = stanza.getChild("fold")
                    let u = jid(stanza.attrs.from);
                    if (stanza.getChildren("error").length) {
                        this.emit("foldError", {
                            game: `${u.bare()}`,
                            user: u.resource,
                        });
                    } else {
                        this.emit("fold", {
                            game: `${u.bare()}`,
                            user: u.resource,
                            player: fold.attrs.player,
                            nickname: jid(stanza.attrs.from).resource
                        });
                    }
                }
                if (stanza.getChildren("game").length) {
                    let game = stanza.getChild("game");
                    if (game.attrs.xmlns != "http://jabber.org/protocol/mug#user") {
                        console.warn(`Dropping unrecognized stanza: ${stanza}`);
                    }
                    let invited = game.getChild("invited");
                    if (invited && invited.attrs.var == this.standard) {
                        let reason = invited.getChild("reason") || null;
                        if (reason) reason = reason.text();
                        let role = null;
                        if (game.getChild("item")) {
                            role = game.getChild("item").attrs.role || null;
                        }
                        this.emit("invited", {
                            invitor: invited.attrs.from,
                            invitee: stanza.attrs.to,
                            role: role,
                            game: stanza.attrs.from,
                            reason: reason,
                        });
                    }
                    let declined = game.getChild("declined");
                    if (declined) {
                        let reason = declined.getChild("reason") || null;
                        if (reason) reason = reason.text();
                        let role = null;
                        if (game.getChild("item")) {
                            role = game.getChild("item").attrs.role || null;
                        }
                        this.emit("invite-declined", {
                            invitee: stanza.attrs.from,
                            invitor: declined.attrs.to,
                            game: stanza.attrs.from,
                            reason: reason,
                            role: role,
                        });
                    }
                }
                if (
                    stanza.attrs.type == "groupchat" &&
                    stanza.getChildren("body").length
                ) {
                    let sender = stanza.attrs.from.split("/");
                    this.emit("groupchat", {
                        from: stanza.attrs.from,
                        to: stanza.attrs.to,
                        user: sender[1],
                        game: sender[0],
                        id: stanza.attrs.id,
                        text: stanza.getChild("body").text(),
                    });
                } else if (
                    stanza.attrs.type == "chat" &&
                    stanza.getChildren("body").length
                ) {
                    this.emit("chat", {
                        from: stanza.attrs.from,
                        to: stanza.attrs.to,
                        id: stanza.attrs.id,
                        text: stanza.getChild("body").text(),
                        nickname: jid(stanza.attrs.from).resource,
                    });
                }
            }
        });
        connection.on("online", async (address) => {
            console.log("...and we're on!");
            this.jid = this.xmpp.jid;
            await connection.send(
                xml(
                    "presence",
                    null,
                    xml("show", null, "chat"),
                    xml("status", null, "Hi, I'm a test user")
                )
            );
            console.log("connected to xmpp server");
            this.emit("connected", {
                status: "ok",
            });
        });
        console.log("All set up. Trying to connect...");
        connection.start().catch(console.error);
    }
    async xmppStop() {
        return await this.xmpp.stop();
    }
    async xmppDiscoverServices() {
        console.log("discovering services");
        let response = await this.xmpp.iqCaller.request(
            xml(
                "iq",
                {
                    type: "get",
                },
                xml("query", {
                    xmlns: "http://jabber.org/protocol/disco#info",
                })
            )
        );
        if (
            response.getChild("query").children.some((e) => {
                return (
                    e.name == "feature" &&
                    e.attrs.var == "http://jabber.org/protocol/disco#items"
                );
            })
        ) {
            console.log("disco#items supported");
            response = await this.xmpp.iqCaller.request(
                xml(
                    "iq",
                    {
                        type: "get",
                    },
                    xml("query", {
                        xmlns: "http://jabber.org/protocol/disco#items",
                    })
                )
            );
            var hosts = [];
            for await (let service of response.getChild("query").children) {
                if (service.name == "item") {
                    console.log(`checking: ${service.attrs.jid}`);
                    try {
                        response = await this.xmpp.iqCaller.request(
                            xml(
                                "iq",
                                {
                                    type: "get",
                                    to: service.attrs.jid,
                                },
                                xml("query", {
                                    xmlns: "http://jabber.org/protocol/disco#info",
                                })
                            )
                        );
                        if (
                            response.getChild("query").children.some((feature) => {
                                return (
                                    feature.name == "feature" &&
                                    feature.attrs.var == this.standard
                                );
                            })
                        ) {
                            console.log(`Found game service at ${service.attrs.jid}`);
                            hosts.push(service.attrs.jid);
                        }
                    } catch (e) {
                        console.log(`Warning: skipping ${service.attrs.jid}: ${e}`);
                    }
                }
            }
            if (hosts.length < 1) {
                console.log("Could not find any hosts for this game :(");
                return;
            }
            console.log(`Found hosts: ${hosts}`);
            this.gameService = hosts[0];
            return this.gameService;
        } else {
            console.log("disco#items not supported here :(");
            return;
        }
    }
    async join(gameID, role, nick = undefined) {
        if (!gameID) throw "Please specify a gameID";
        if (gameID.indexOf("@") == -1) gameID = `${gameID}@${this.gameService}`;
        gameID = jid(gameID);
        if (nick) gameID.resource = nick;
        this.xmpp.send(
            xml(
                "presence",
                {
                    from: this.jid,
                    to: gameID,
                },
                xml(
                    "game",
                    {
                        var: this.standard,
                    },
                    xml("item", {
                        role: role,
                    })
                )
            )
        );
    }
    kibitz(gameID, role, nick = undefined) {
        if (!gameID) throw "Please specify a gameID";
        if (gameID.indexOf("@") == -1) gameID = `${gameID}@${this.gameService}`;
        gameID = jid(gameID);
        if (nick) gameID.resource = nick;
        // Send IQ stanza to change the user's role
        this.xmpp.send(
            xml(
                "iq",
                {
                    from: this.jid,
                    type: "set",
                    to: gameID,
                    id: "change-watch",
                },
                xml(
                    "query",
                    {
                        xmlns: "http://jabber.org/protocol/mug#owner",
                    },
                    xml("item", {
                        jid: this.jid,
                        role: role,
                    })
                )
            )
        );
    }
    leave(gameID) {
        if (!gameID) throw "Please specify a gameID";
        if (gameID.indexOf("@") == -1) gameID = `${gameID}@${this.gameService}`;
        this.xmpp.send(
            xml(
                "presence",
                {
                    from: this.jid,
                    to: gameID,
                    type: "unavailable",
                },
                xml(
                    "game",
                    {
                        xmlns: "http://jabber.org/protocol/mug",
                    },
                    xml("item", {
                        affiliation: "none",
                        role: "none",
                        jid: this.jid,
                    })
                )
            )
        );
    }
    start(gameID) {
        if (!gameID) throw "Please specify a gameID";
        if (gameID.indexOf("@") < 0) gameID = `${gameID}@${this.gameService}`;
        this.xmpp.send(
            xml(
                "message",
                {
                    from: this.jid,
                    to: gameID,
                },
                xml("start", {
                    xmlns: "http://jabber.org/protocol/mug#user",
                })
            )
        );
    }
    chat(userJID, text) {
        if (!userJID) throw "Please specify a user JID!";
        this.xmpp.send(
            xml(
                "message",
                {
                    from: this.jid,
                    to: userJID,
                    type: "chat",
                },
                xml("body", null, text)
            )
        );
    }
    groupChat(gameID, text) {
        if (!gameID) throw "Please specify a gameID";
        if (gameID.indexOf("@") == -1) gameID = `${gameID}@${this.gameService}`;
        this.xmpp.send(
            xml(
                "message",
                {
                    from: this.jid,
                    to: gameID,
                    type: "groupchat",
                },
                xml("body", null, text)
            )
        );
    }
    fold(gameID) {
        if (!gameID) throw "Please specify a gameID";
        if (gameID.indexOf("@") < 0) gameID = `${gameID}@${this.gameService}`;
        this.xmpp.send(
            xml(
                "message",
                {
                    from: this.jid,
                    to: gameID,
                },
                xml("fold", {
                    xmlns: "http://jabber.org/protocol/mug#user",
                })
            )
        );
    }
    claim(gameID, claim, mode = undefined) {
        if (!gameID) throw 'Please specify a gameID';
        if (!claim) throw 'Please specify a claim object';

        if (gameID.indexOf('@') < 0) gameID = `${gameID}@${this.gameService}`;

        // Check if claimObj is an object
        if (claim && typeof claim === 'object') {

            // Construct XML for the claim object
            const claimXml = Object.keys(claim).map(playerIndex => {
                const claimValue = claim[playerIndex];
                return xml("claim", { playerIndex: playerIndex, trickCount: claimValue });
            });

            // Construct the message with claim XML
            const messageXml = xml("message", {
                from: this.jid,
                to: gameID,
                type: "chat"
            }, xml("turn", {
                xmlns: "http://jabber.org/protocol/mug#user"
            }, xml("claim", {
                xmlns: this.standard,
                mode: mode
            }, claimXml)));

            // Send the message
            this.xmpp.send(messageXml);
        } else {
            console.error("claimObj is not properly initialized or is not an object");
        }
    }

    swap(gameID, swap, no_swap = undefined) {
        if (!gameID) throw 'Please specify a gameID';
        if (!swap || !Array.isArray(swap)) throw 'Please specify a valid swap array';

        if (gameID.indexOf('@') < 0) gameID = `${gameID}@${this.gameService}`;

        if (!no_swap) {
            // Construct XML for the swap objects
            const swapXml = swap.map(swapObj => {
                const { from, to, subHand } = swapObj;
                const subHandXml = subHand.map(card => xml("card", {
                    suit: card.suit,
                    rank: card.rank,
                    xmlns: this.standard
                }));

                return xml("hand", { from, to }, xml("subHand", null, subHandXml));
            });


            // Construct the message with swap XML
            const messageXml = xml("message", {
                from: this.jid,
                to: gameID,
                type: "chat"
            }, xml("turn", {
                xmlns: "http://jabber.org/protocol/mug#user"
            }, xml("swap", {
                xmlns: this.standard
            }, swapXml)));


            // Send the message
            this.xmpp.send(messageXml);
        } else {
            const messageXml = xml("message", {
                from: this.jid,
                to: gameID,
                type: "chat"
            }, xml("turn", {
                xmlns: "http://jabber.org/protocol/mug#user"
            }, xml("swap", {
                xmlns: this.standard,
                no_swap: no_swap
            })));

            // Send the message
            this.xmpp.send(messageXml);
        }
    }

    undo(gameID, mode, card = undefined) {
        console.log("Card value to server ", card)
        if (!gameID) throw "Please specify a gameID";
        if (gameID.indexOf("@") < 0) gameID = `${gameID}@${this.gameService}`;

        this.xmpp.send(xml("message", {
            from: this.jid,
            to: gameID,
            type: "chat"
        }, xml("turn", {
            xmlns: "http://jabber.org/protocol/mug#user"
        }, xml("undo", {
            xmlns: this.standard,
            mode: mode,
        }, xml("card", {
            suit: card.card.suit,
            rank: card.card.rank,
            player: card.player,
        })
        ))));
    }
}
