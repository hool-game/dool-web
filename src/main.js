import "./app.css";
import App from "./App.svelte";

// For Node <-> browser compatability
import "core-js/stable";
import "regenerator-runtime/runtime";

import { DoolClient } from "dool-client";
import Dool from "./dool.js";

// Set the default login and resource
let defaultLogin = import.meta.env.VITE_XMPP_DOMAIN || "faeko.hool.org";

let resource = import.meta.env.VITE_XMPP_RESOURCE || "faeko-lite";

let client = new DoolClient({
  service: import.meta.env.VITE_XMPP_WEBSOCKET,
  resource: resource,
});

if (import.meta.env.VITE_XMPP_GAME_SERVICE) {
  client.gameService = import.meta.env.VITE_XMPP_GAME_SERVICE;
}

let dool = new Dool({}, client);

const app = new App({
  target: document.getElementById("app"),
  props: {
    client,
    dool,
    defaultLogin,
  },
});

// Allow for debugging if needed
if (import.meta.env.VITE_DEBUG) {
  window.dool = {
    client,
    dool,
    app,
  };
}

export default app;
