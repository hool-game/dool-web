// store.js
import { writable } from 'svelte/store';

// Create a writable store for disable status
export const disableBtnStore = writable(true); // Initially disabled
