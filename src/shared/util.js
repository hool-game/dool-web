/**
 * For returning team of the particular player
 * @param players players from the game 
 * @param nickname particular player nickname
 */
export function getTeamPlayers(players, nickname) {
    return players && players.filter((player) => player.nickname === nickname)
}

/**
 * 
 * @param {*} teams teams array which will have subset arrays of playerIndices
 * @param {*} playerIndex playerIndex of the player
 * @returns 
 */
export function getTeam(teams, playerIndex) {
    return teams.find(team => team.includes(playerIndex))
}

/**
 * Returns betting related array
 */
export function getBettingArray(handLength) {
    // default betting array
    let bettingArray = [10, 20, 30, 40, 50, 60];
    // default ante value
    let ante = 10
    
    if (handLength == 13) {
        ante = 10;
        bettingArray = [10, 20, 30, 40, 50, 60, 70];
    } else if (handLength == 10) {
        ante = 50;
        bettingArray = [50, 100, 200, 400, 800];
    } else if (handLength == 5) {
        ante = 10;
        bettingArray = [10, 50, 100];
    } else if (handLength == 6) {
        ante = 10;
        bettingArray = [10, 50, 100];
    } else {
        ante = 50;
        bettingArray = [50, 100, 200, 400, 800, 1600];
    }
    return { ante, bettingArray }
}

/**
 * Will return true, if is the player turn?!
 * @param type type of the turn/stage
 * @param players the array includes whichever player's index is
 * @param i the index value from the loop
 */
export function isMyTurn(type, players, i) {
    if (["swap", "card", "take"].includes(type)) {
        return players.includes(i);
    } else if (["bid", "leading"].includes(type)) {
        return players.includes(i) || players.includes(i % 2);
    }
}

/**
 * Will return the first letter of the username
 * @param {*} nickname the player's name value
 * @returns a single Capital letter
 */
export function convertToUpperCase(nickname) {
    return nickname.slice(0, 1).toUpperCase();
}

/**
 * Will return add-up value to the caller
 * @param {*} level bid or contract level
 * @param {*} handLength actual card count the player has at the beginning
 * @returns 
 */
export function getLevel(level, handLength) {
    if (handLength == 13) {
        level = level;
    } else if (handLength == 6) {
        level = level + 3;
    } else if (handLength == 10) {
        level = level + 5;
    } else {
        level = level
    }

    return level
}

/**
 * Will return true, if is the player turn?!
 * @param players the array includes whichever player's index is
 * @param player the current player index
 * @param teams the teams array
 */
export function getMyTurn(players, player, teams) {
    // Find the subteam of the current player
    let subTeam = teams.find(team => team.includes(player));

    // Check if any player in the subteam is in the turn.players array
    let isPlayerTurn = subTeam.some(teamPlayer => players.includes(teamPlayer));

    return isPlayerTurn
}